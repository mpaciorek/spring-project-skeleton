package pl.inpost.payment.gateway.application.service.api;

import pl.inpost.payment.gateway.domain.entity.Transaction;


public interface TransactionService {
    void startTransaction(Transaction transaction);
}
