package pl.inpost.payment.gateway.application.service.config;

import pl.inpost.payment.gateway.application.service.adapter.TransactionServiceAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.inpost.payment.gateway.application.service.api.TransactionService;
import pl.inpost.payment.gateway.domain.spi.TransactionPersistencePort;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public TransactionService getTransactionService(TransactionPersistencePort transactionPersistencePort) {
        return new TransactionServiceAdapter(transactionPersistencePort);
    }
}
