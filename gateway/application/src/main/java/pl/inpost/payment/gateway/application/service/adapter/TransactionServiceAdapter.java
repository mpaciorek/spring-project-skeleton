package pl.inpost.payment.gateway.application.service.adapter;

import pl.inpost.payment.gateway.application.service.api.TransactionService;
import pl.inpost.payment.gateway.domain.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import pl.inpost.payment.gateway.domain.spi.TransactionPersistencePort;


public class TransactionServiceAdapter implements TransactionService {

    private TransactionPersistencePort transactionPersistencePort;

    @Autowired
    public TransactionServiceAdapter(TransactionPersistencePort transactionPersistencePort) {
        this.transactionPersistencePort = transactionPersistencePort;
    }

    @Override
    public void startTransaction(Transaction transaction) {
        transactionPersistencePort.hashCode();
    }
}
