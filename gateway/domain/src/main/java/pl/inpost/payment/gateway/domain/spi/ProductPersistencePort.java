package pl.inpost.payment.gateway.domain.spi;

import pl.inpost.payment.gateway.domain.model.Product;

import java.util.List;

public interface ProductPersistencePort {

    void addProduct(Product product);

    void removeProduct(Product product);

    List<Product> getProducts();

    Product getProductById(Integer productId);
}
