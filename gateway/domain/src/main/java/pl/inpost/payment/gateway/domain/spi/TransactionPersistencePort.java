package pl.inpost.payment.gateway.domain.spi;

import pl.inpost.payment.gateway.domain.entity.Transaction;

public interface TransactionPersistencePort {
    void addTransaction(Transaction transaction);
}
