package pl.inpost.payment.gateway.domain.entity;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;

public class Transaction {

    public enum Status {
        INITIALIZED, PENDING, COMPLETED, CANCELLED
    }

    String transactionId;
    Status status = Status.INITIALIZED;
    Money amount;


    public Transaction() {
    }


    public Status getStatus() {
        return status;
    }
}
