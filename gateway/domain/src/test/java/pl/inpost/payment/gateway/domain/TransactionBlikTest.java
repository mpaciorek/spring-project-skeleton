package pl.inpost.payment.gateway.domain;

import org.junit.jupiter.api.Test;
import pl.inpost.payment.gateway.domain.entity.Transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TransactionBlikTest {

    @Test
    void initialStatusCheck() {
        Transaction transaction = new Transaction();

        assertEquals(transaction.getStatus(), Transaction.Status.INITIALIZED);
    }
}
