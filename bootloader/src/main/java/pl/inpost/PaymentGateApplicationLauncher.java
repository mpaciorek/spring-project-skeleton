package pl.inpost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentGateApplicationLauncher {
    public static void main(String[] args) {
        SpringApplication.run(PaymentGateApplicationLauncher.class, args);
    }
}
